<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuktisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buktis', function (Blueprint $table) {
            $table->id();
            $table->foreignId('team_id');
            $table->string('bukti_share_wa_0')->nullable();
            $table->string('bukti_share_wa_1')->nullable();
            $table->string('bukti_share_wa_2')->nullable();
            $table->string('bukti_follow_instagram_0')->nullable();
            $table->string('bukti_follow_instagram_1')->nullable();
            $table->string('bukti_follow_instagram_2')->nullable();
            $table->string('bukti_twibbon_0')->nullable();
            $table->string('bukti_twibbon_1')->nullable();
            $table->string('bukti_twibbon_2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buktis');
    }
}
