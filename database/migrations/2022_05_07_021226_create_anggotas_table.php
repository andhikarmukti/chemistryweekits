<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnggotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggotas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ketua_id');
            $table->string('nama_lengkap');
            $table->string('nim');
            $table->string('jurusan');
            $table->string('no_hp');
            $table->string('email')->unique();
            $table->string('ktm');
            $table->string('pas_photo');
            $table->string('link_twibbon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggotas');
    }
}
