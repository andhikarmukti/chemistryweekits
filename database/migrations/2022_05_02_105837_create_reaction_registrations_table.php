<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReactionRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reaction_registrations', function (Blueprint $table) {
            $table->id();
            // $table->string('asal_institusi');
            // $table->enum('jabatan', ['ketua', 'anggota']);
            // $table->foreignId('team_id');
            // $table->string('nama_lengkap');
            // $table->string('nim');
            // $table->string('jurusan');
            // $table->string('no_hp');
            // $table->string('email')->unique();
            // $table->string('judul_karya_tulis');
            // $table->string('subtema');
            // $table->string('ktm');
            // $table->string('pas_photo');
            // $table->string('bukti_share_wa');
            // $table->string('bukti_follow_instagram');
            // $table->string('bukti_twibbon');
            // $table->string('bukti_link_twibbon');
            // $table->string('file_abstrak');
            // $table->string('contact_person');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reaction_registrations');
    }
}
