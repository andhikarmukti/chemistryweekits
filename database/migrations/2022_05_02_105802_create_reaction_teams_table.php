<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReactionTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reaction_teams', function (Blueprint $table) {
            $table->id();
            $table->string('asal_institusi');
            $table->string('nama_team')->unique();
            $table->foreignId('jenis_lomba_id');
            $table->string('contact_person');
            // $table->string('bukti_pembayaran')->nullable();
            $table->enum('tahap_seleksi', [1,2,3])->default(1);
            $table->boolean('tidak_lulus')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reaction_teams');
    }
}
