<?php

namespace Database\Seeders;

use App\Models\JenisLomba;
use Illuminate\Database\Seeder;

class JenisLombaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JenisLomba::create([
            'nama_lomba' => 'reaction'
        ]);
        JenisLomba::create([
            'nama_lomba' => 'ncc'
        ]);
        JenisLomba::create([
            'nama_lomba' => 'spectrum'
        ]);
    }
}
