<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\JenisLomba;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'name' => 'Jaka Thinker',
            'email' => 'jk@gmail.com',
            'jenis_lomba_id' => 1,
            'password' => Hash::make('asdasdasd'),
            'role' => 'admin'
        ]);
        User::create([
            'name' => 'Andhika Raharja Mukti',
            'email' => 'arm@gmail.com',
            'jenis_lomba_id' => 1,
            'password' => Hash::make('asdasdasd'),
        ]);
        User::create([
            'name' => 'Wiro Kesableng',
            'email' => 'wk@gmail.com',
            'jenis_lomba_id' => 1,
            'password' => Hash::make('asdasdasd'),
        ]);
        $this->call([
            JenisLombaSeeder::class
        ]);
    }
}
