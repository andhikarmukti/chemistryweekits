<?php

use App\Http\Controllers\BuktiController;
use App\Models\User;
use App\Models\Bukti;
use App\Models\Ketua;
use App\Models\Anggota;
use App\Models\KaryaTulis;
use App\Models\ReactionTeam;
use Illuminate\Http\Request;
use App\Models\ReactionRegistration;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\KetuaController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\PemenangController;
use App\Http\Controllers\KaryaTulisController;
use App\Http\Controllers\ReactionTeamController;
use App\Http\Controllers\ReactionRegistrationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:sanctum'], function(){
    // Reaction
    Route::post('/reaction-form-update', [ReactionRegistrationController::class, 'update']);
    Route::post('/reaction-submit', [ReactionRegistrationController::class, 'store']);
    Route::post('/reaction-submit-2', [KaryaTulisController::class, 'store2']);
    Route::post('/reaction-submit-3', [KaryaTulisController::class, 'store3']);
    Route::get('/reaction-users', [UserController::class, 'reactionUserFetch']);
    Route::get('/reaction-ketua', [KetuaController::class, 'reactionKetuaFetch']);
    Route::get('/reaction-team', [ReactionTeamController::class, 'showFinalis']);
    Route::post('/reaction-pemenang-1', [ReactionTeamController::class, 'pemenangPertama']);
    Route::post('/reaction-pemenang-2', [ReactionTeamController::class, 'pemenangKedua']);
    Route::post('/reaction-tidak-lulus', [ReactionTeamController::class, 'tidakLulus']);
    Route::post('/reaction-konfirmasi-pembayaran/{reactionTeam}', [InvoiceController::class, 'confirm']);
    Route::post('/reaction-pemenang', [PemenangController::class, 'pemenang']);
    Route::get('/reaction-pemenang', [PemenangController::class, 'fetchPemenang']);
    Route::get('/reaction-bukti', [BuktiController::class, 'fetchBukti']);
    Route::get('/reaction-message', [UserController::class, 'homeMessage']);

    Route::get('/nominal', function(){
        if(strtotime(today()) >= strtotime("2022-06-26 23:59:59") && strtotime(today()) <= strtotime("2022-08-07 23:59:59")){
            $nominal = 120000;
        }else if(strtotime(today()) >= strtotime("2022-08-07 23:59:59") && strtotime(today()) <= strtotime("2022-09-11 23:59:59")){
            $nominal = 150000;
        }else{
            $nominal = 0;
        }

        return response()->json([
            'status' => 'success',
            'nominal' => $nominal
        ], 200);
    });

    // Invoice
    // Route::post('/invoice-submit', [InvoiceController::class, 'store']);
});

Route::get('/dd', function(){
    // $path = public_path('images/16532931015.png');
    // $isExists = File::exists($path);
    // dd($isExists);

    // $arr = [];
    // foreach(auth()->user()->team->buktis as $bukti){
    //     array_push($arr, $bukti->bukti_share_wa);
    // }

    return response()->json([
        'data' => str_replace(' ', '', 'Andhika Raharja Mukti')
    ]);
});
// Register & Login
Route::post('/register', [UserController::class, 'register']);
Route::post('/login', [UserController::class, 'login']);


Route::get('/reset', function(){
    Ketua::truncate();
    Anggota::truncate();
    ReactionTeam::truncate();
    Bukti::truncate();
    KaryaTulis::truncate();
    for($i = 1; $i < 100; $i++){
        User::where('id', $i)->update([
            'team_id' => NULL
        ]);
    }
    return 'reset ok';
});
