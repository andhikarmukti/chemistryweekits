<?php

namespace App\Http\Controllers;

use App\Models\Ketua;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreKetuaRequest;
use App\Http\Requests\UpdateKetuaRequest;

class KetuaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreKetuaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreKetuaRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ketua  $ketua
     * @return \Illuminate\Http\Response
     */
    public function reactionKetuaFetch()
    {
        if(!Gate::allows('admin')){
            $ketua = Ketua::where('team_id', auth()->user()->team_id)->with('anggotas')
            ->with('team', 'team.buktis', 'team.karyaTulis', 'team.invoice')
            ->get();

            return response()->json([
                'status' => 'success',
                'user' => auth()->user(),
                'data' => $ketua
            ], 200);
        }

        $ketua = Ketua::with('anggotas')
            ->with('team', 'team.buktis', 'team.karyaTulis', 'team.invoice')
            ->get();

        return response()->json([
            'status' => 'success',
            'user' => auth()->user(),
            'data' => $ketua
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ketua  $ketua
     * @return \Illuminate\Http\Response
     */
    public function edit(Ketua $ketua)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateKetuaRequest  $request
     * @param  \App\Models\Ketua  $ketua
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateKetuaRequest $request, Ketua $ketua)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ketua  $ketua
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ketua $ketua)
    {
        //
    }
}
