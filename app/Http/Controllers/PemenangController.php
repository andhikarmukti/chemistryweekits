<?php

namespace App\Http\Controllers;

use App\Models\Pemenang;
use App\Http\Requests\StorePemenangRequest;
use App\Http\Requests\UpdatePemenangRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PemenangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePemenangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePemenangRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pemenang  $pemenang
     * @return \Illuminate\Http\Response
     */
    public function show(Pemenang $pemenang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pemenang  $pemenang
     * @return \Illuminate\Http\Response
     */
    public function edit(Pemenang $pemenang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePemenangRequest  $request
     * @param  \App\Models\Pemenang  $pemenang
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePemenangRequest $request, Pemenang $pemenang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pemenang  $pemenang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pemenang $pemenang)
    {
        //
    }

    public function pemenang(Request $request)
    {
        $pemenang = new Pemenang;
        foreach($request->all() as $team_id){
           $team_id_check = $pemenang->where('juara_ke', $team_id)->first();
           if($team_id_check == null){
            return response()->json([
                'status' => 'error',
                'message' => 'Anda telah memasukan salah satu team yang tidak terdaftar'
            ], 400);
           }
        }

        foreach($request->all() as $juara => $team_id){
            Pemenang::updateOrCreate([
                'juara_ke' => $juara
            ],
            [
                'team_id' => $team_id,
                'juara_ke' => $juara
            ]);
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil submit ketiga juara',
        ], 200);
    }

    public function fetchPemenang()
    {
        $pemenangs = Pemenang::all();
        if($pemenangs->first() == null){
            return response()->json([
                'message' => 'Belum ada pemilihan untuk juara pertama, kedua dan ketiga'
            ], 200);
        }
        return response()->json([
            'status' => 'success',
            'data' => [
                'juara_1' => $pemenangs->where('juara_ke', 1)->first()->team->nama_team,
                'juara_2' => $pemenangs->where('juara_ke', 2)->first()->team->nama_team,
                'juara_3' => $pemenangs->where('juara_ke', 3)->first()->team->nama_team,
            ]
        ], 200);
    }
}
