<?php

namespace App\Http\Controllers;

use App\Models\JenisLomba;
use App\Http\Requests\StoreJenisLombaRequest;
use App\Http\Requests\UpdateJenisLombaRequest;

class JenisLombaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreJenisLombaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJenisLombaRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JenisLomba  $jenisLomba
     * @return \Illuminate\Http\Response
     */
    public function show(JenisLomba $jenisLomba)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JenisLomba  $jenisLomba
     * @return \Illuminate\Http\Response
     */
    public function edit(JenisLomba $jenisLomba)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateJenisLombaRequest  $request
     * @param  \App\Models\JenisLomba  $jenisLomba
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateJenisLombaRequest $request, JenisLomba $jenisLomba)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JenisLomba  $jenisLomba
     * @return \Illuminate\Http\Response
     */
    public function destroy(JenisLomba $jenisLomba)
    {
        //
    }
}
