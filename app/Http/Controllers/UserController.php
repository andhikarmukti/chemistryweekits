<?php

namespace App\Http\Controllers;

use App\Models\Pemenang;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private function notif($user)
    {
        $token = 'Y1fKzTE7eDHYSuwXdcfpbww96CkexcPjSL5hMFVrEBwYMpNyfC';
        $message = 'Ada pendaftar akun baru dengan email ' . $user->email . ' untuk lomba ' . $user->jenis_lomba->nama_lomba;
        $hp = '62895630957547';
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://app.ruangwa.id/api/send_message',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => [
            'token' => $token,
            'number' => $hp,
            'message' => $message
        ],
        ));
        curl_exec($curl);
        curl_close($curl);

        return $hp;
    }

    public function register(Request $request)
    {
        $rules = [
            'name' => 'required',
            'password' => 'required|min:6',
            'jenis_lomba_id' => 'required',
            'email' => 'required|email:dns|unique:users,email',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'failed',
                'message' => $validator->errors()
            ], 400);
        }

        try{
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'jenis_lomba_id' => $request->jenis_lomba_id,
            ]);

            $notif = $this->notif($user);

            return response()->json([
                'status' => 'success',
                'message' => 'Daftar akun berhasil! Silahkan login.'
            ], 200);
        }catch(\Exception $e){
            throw $e;
        }
    }

    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email:dns',
            'password' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'failed',
                'message' => $validator->errors()
            ], 400);
        }

        if (!Auth::attempt($request->only('email', 'password')))
        {
            return response()->json([
                'status' => 'failed',
                'message' => 'Email atau password salah'
            ], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken($user->id)->plainTextToken;

        return response()
            ->json([
                'status' => 'success',
                'message' => $user->name . ' Berhasil login!',
                'access_token' => $token,
                'token_type' => 'Bearer',
                'role' => $user->role
            ]);
    }

    public function reactionUserFetch()
    {
        if(Gate::allows('admin')){
            $users = User::all();
            return response()->json([
                'status' => 'success',
                'data' => $users
            ], 200);
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Anda bukan admin'
        ], 403);
    }

    public function homeMessage()
    {
        $message = 'Kamu sudah mengisi form pendaftaran Reaction';

        if(Gate::allows('admin')){
            $message = 'Hallo Admin!';
            return response()->json([
                'message' => $message
            ], 200);
        }

        // Belum Isi Form (bukan Admin)
        if(!auth()->user()->team_id && auth()->user()->role != 'admin'){
            $message = 'Kamu belum mengisi form pendaftaran Reaction';
            return response()->json([
                'message' => $message
            ], 200);
        }

        // Tidak Lulus
        if(!Gate::allows('admin')){
            if(auth()->user()->team->tidak_lulus == 1){
                $message = 'Tetap semangat! Banyak yang bilang kegagalan adalah keberhasilan yang tertunda.';
                return response()->json([
                    'message' => $message
                ], 200);
            }
        }

        // Juara 1 2 3
        if(Pemenang::first() != null){
            if(auth()->user()->team->pemenang->juara_ke == 1){
                $message = 'Selamat kamu menjadi juara 1!';
                return response()->json([
                    'message' => $message
                ], 200);
            }
            if(auth()->user()->team->pemenang->juara_ke == 2){
                $message = 'Selamat kamu menjadi juara 2!';
                return response()->json([
                    'message' => $message
                ], 200);
            }
            if(auth()->user()->team->pemenang->juara_ke == 3){
                $message = 'Selamat kamu menjadi juara 3!';
                return response()->json([
                    'message' => $message
                ], 200);
            }
        }

        // Tahap 2
        if(!Gate::allows('admin')){
            if(auth()->user()->team->tahap_seleksi == 2 && auth()->user()->team->karyaTulis->file_full_paper == null){
                $message = 'Kamu belum upload file full paper';
                return response()->json([
                    'message' => $message
                ], 200);
            }
            if(auth()->user()->team->tahap_seleksi == 2 && !auth()->user()->team->karyaTulis->file_full_paper == null){
                $message = 'Kamu sudah upload file full paper';
                return response()->json([
                    'message' => $message
                ], 200);
            }

            // Tahap 3
            if(auth()->user()->team->tahap_seleksi == 3 && auth()->user()->team->karyaTulis->file_power_point == null){
                $message = 'Kamu belum upload file power point';
                return response()->json([
                    'message' => $message
                ], 200);
            }
            if(auth()->user()->team->tahap_seleksi == 3 && !auth()->user()->team->karyaTulis->file_power_point == null){
                $message = 'Kamu sudah upload file power point';
                return response()->json([
                    'message' => $message
                ], 200);
            }
        }

        return response()->json([
            'message' => $message
        ], 200);
    }
}
