<?php

namespace App\Http\Controllers;

use App\Models\Bukti;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreBuktiRequest;
use App\Http\Requests\UpdateBuktiRequest;

class BuktiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBuktiRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBuktiRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bukti  $bukti
     * @return \Illuminate\Http\Response
     */
    public function show(Bukti $bukti)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bukti  $bukti
     * @return \Illuminate\Http\Response
     */
    public function edit(Bukti $bukti)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBuktiRequest  $request
     * @param  \App\Models\Bukti  $bukti
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBuktiRequest $request, Bukti $bukti)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bukti  $bukti
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bukti $bukti)
    {
        //
    }

    public function fetchBukti()
    {
        $buktis = DB::table('buktis')->select('team_id as team_id', 'bukti_share_wa as WA', 'bukti_follow_instagram as IG', 'bukti_twibbon as TWB')
        // ->groupBy('team_id')
        ->get();

        return response()->json([
            'status' => 'success',
            'data' => $buktis
        ], 200);
    }
}
