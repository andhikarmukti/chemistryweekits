<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\KaryaTulis;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreKaryaTulisRequest;
use App\Http\Requests\UpdateKaryaTulisRequest;
use App\Models\ReactionTeam;

class KaryaTulisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreKaryaTulisRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreKaryaTulisRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KaryaTulis  $karyaTulis
     * @return \Illuminate\Http\Response
     */
    public function show(KaryaTulis $karyaTulis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KaryaTulis  $karyaTulis
     * @return \Illuminate\Http\Response
     */
    public function edit(KaryaTulis $karyaTulis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateKaryaTulisRequest  $request
     * @param  \App\Models\KaryaTulis  $karyaTulis
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateKaryaTulisRequest $request, KaryaTulis $karyaTulis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KaryaTulis  $karyaTulis
     * @return \Illuminate\Http\Response
     */
    public function destroy(KaryaTulis $karyaTulis)
    {
        //
    }

    public function store2(Request $request)
    {
        // Pengecekan apakah team ini lulus tahap 1
        if(auth()->user()->team->tahap_seleksi < 2 || auth()->user()->team->tidak_lulus === 1){
            return response()->json([
                'status'  => 'error',
                'message' => 'Maaf, team kamu tidak lulus tahap 1'
            ], 403);
        }

        $rules = [
            'nama_pemilik_rekening' => 'required',
            'bank' => 'required',
            'tanggal_transfer' => 'required',
            'file_full_paper' => 'required|mimes:pdf',
            'bukti_transfer' => 'required|mimes:jpeg,png,jpg'
        ];
        $message = [
            'required' => 'Tidak boleh kosong',
            'mimes:pdf' => 'Format file harus PDF',
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()){
            return response()->json([
                'status'  => 'failed',
                'message' => $validator->errors()
            ], 400);
        }

        DB::beginTransaction();
        try{
            $full_paper_name = 'FullPaper-' . str_replace(' ', '', auth()->user()->team->nama_team) . Str::random(6) . '.' . $request->file_full_paper->extension();
            KaryaTulis::where('team_id', auth()->user()->team_id)->update([
                'file_full_paper' => $full_paper_name
            ]);

            $bukti_transfer_name = 'BuktiTransfer-' . str_replace(' ', '', auth()->user()->team->nama_team) . Str::random(6) . '.' . $request->bukti_transfer->extension();
            $no_invoice = 'Reaction-' . str_replace(' ', '', auth()->user()->team->nama_team) . Str::random(6) . today()->format('Y-m-d');

            // Menentukan nominal
            if(strtotime(today()) >= strtotime("2022-06-26 23:59:59") && strtotime(today()) <= strtotime("2022-08-07 23:59:59")){
                $nominal = 120000;
            }else if(strtotime(today()) >= strtotime("2022-08-07 23:59:59") && strtotime(today()) <= strtotime("2022-09-11 23:59:59")){
                $nominal = 150000;
            }else{
                $nominal = 0;
            }

            if(Invoice::where('team_id', auth()->user()->team_id)->first()){
                $invoice = Invoice::where('team_id', auth()->user()->team_id)->update([
                    'team_id' => auth()->user()->team->id,
                    'no_invoice' => $no_invoice,
                    'bukti_transfer' => $bukti_transfer_name
                ]);
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil update data!',
                ], 200);
            }else{
                $invoice = Invoice::create([
                    'team_id' => auth()->user()->team->id,
                    'no_invoice' => trim($no_invoice),
                    'nominal' => $nominal,
                    'nama_pemilik_rekening' => $request->nama_pemilik_rekening,
                    'bank' => $request->bank,
                    'tanggal_transfer' => $request->tanggal_transfer,
                    'bukti_transfer' => $bukti_transfer_name
                ]);
            }
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            throw $e;
        }

        $request->file_full_paper->move(public_path('full_paper'), $full_paper_name);
        $request->bukti_transfer->move(public_path('bukti_transfer'), $bukti_transfer_name);
        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil upload!',
            'no invoice' => $invoice->no_invoice
        ], 200);
    }

    public function store3(Request $request)
    {
        // Pengecekan apakah team ini lulus tahap 1
        if(auth()->user()->team->tahap_seleksi < 3 || auth()->user()->team->tidak_lulus === 1){
            return response()->json([
                'status'  => 'error',
                'message' => 'Maaf, team kamu tidak lulus tahap 1'
            ], 403);
        }

        $rules = [
            'file_power_point' => 'required|mimes:pptx'
        ];
        $message = [
            'required' => 'Tidak boleh kosong!',
            'mimes:pptx' => 'Format file harusnya pptx',
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()){
            return response()->json([
                'status' => 'succes',
                'message' => $validator->errors()
            ], 400);
        }

        $power_point_name = 'PPT-' . Str::random(6) . today()->format('Y-m-d') . $request->file_power_point->extension();
        $request->file_power_point->move(public_path('power_point'), $power_point_name);
        KaryaTulis::where('team_id', auth()->user()->team_id)->update([
            'file_power_point' => $power_point_name
        ]);
        return response()->json([
            'status' => 'succes',
            'message' => 'Berhasil upload file!'
        ], 200);
    }
}
