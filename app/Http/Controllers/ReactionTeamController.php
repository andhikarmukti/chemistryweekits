<?php

namespace App\Http\Controllers;

use App\Models\ReactionTeam;
use App\Http\Requests\StoreReactionTeamRequest;
use App\Http\Requests\UpdateReactionTeamRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ReactionTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreReactionTeamRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReactionTeamRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReactionTeam  $reactionTeam
     * @return \Illuminate\Http\Response
     */
    public function showFinalis()
    {
        if(!Gate::allows('admin')){
            return response()->json([
                'status' => 'success',
                'message' => 'Anda bukan admin!'
            ], 403);
        }

        $teams = ReactionTeam::where('tidak_lulus', 0)->get();
        return response()->json([
            'status' => 'success',
            'data' => $teams
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReactionTeam  $reactionTeam
     * @return \Illuminate\Http\Response
     */
    public function edit(ReactionTeam $reactionTeam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateReactionTeamRequest  $request
     * @param  \App\Models\ReactionTeam  $reactionTeam
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReactionTeamRequest $request, ReactionTeam $reactionTeam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReactionTeam  $reactionTeam
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReactionTeam $reactionTeam)
    {
        //
    }

    public function pemenangPertama(Request $request)
    {
        if(Gate::allows('admin')){
            ReactionTeam::where('id', $request->id)->update([
                'tahap_seleksi' => 2
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'Selamat team ' . ReactionTeam::find($request->id)->nama_team . ' lolos menuju tahap ke-2'
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'message' => 'Anda bukan admin'
        ], 403);
    }

    public function pemenangKedua(Request $request)
    {
        if(Gate::allows('admin')){
            ReactionTeam::where('id', $request->id)->update([
                'tahap_seleksi' => 3
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'Selamat team ' . ReactionTeam::find($request->id)->nama_team . ' lolos menuju tahap ke-3'
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'message' => 'Anda bukan admin'
        ], 403);
    }

    public function tidakLulus(Request $request)
    {
        $nama_team = ReactionTeam::find($request->id)->nama_team;
        if(Gate::allows('admin')){
            ReactionTeam::where('id', $request->id)->update([
                'tidak_lulus' => TRUE
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'Team ' . $nama_team . ' tidak lulus'
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'message' => 'Anda bukan admin'
        ], 403);
    }
}
