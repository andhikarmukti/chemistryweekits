<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Bukti;
use App\Models\Ketua;
use App\Models\Anggota;
use App\Models\KaryaTulis;
use Illuminate\Support\Str;
use App\Models\ReactionTeam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ReactionRegistration;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreReactionRegistrationRequest;
use App\Http\Requests\UpdateReactionRegistrationRequest;

class ReactionRegistrationController extends Controller
{

    public $message;
    public $response = [];
    public $err = [];
    public function __construct()
    {
        $this->message = [
            'required' => 'Tidak boleh kosong!',
            'email' => 'Harus format email',
            'mimes:pdf' => 'Harus format PDF',
            'jpeg' => 'Harus format JPEG'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreReactionRegistrationRequest  $request
     * @return \Illuminate\Http\Response
     */

    public function teamStore($teamParams)
    {
        $rules = [
            'nama_team' => 'required|unique:reaction_teams,nama_team',
            'asal_institusi' => 'required',
            'jenis_lomba_id' => 'required',
            'contact_person' => 'required'
        ];

        $validator = Validator::make($teamParams, $rules, $this->message);

        if($validator->fails()){
            $this->err['status'] = 'error';
            $this->err['message'] = $validator->errors()->first();
            return $this->err;
        }

        $team = ReactionTeam::create([
            'nama_team' => $teamParams['nama_team'],
            'asal_institusi' => $teamParams['asal_institusi'],
            'jenis_lomba_id' => $teamParams['jenis_lomba_id'],
            'contact_person' => $teamParams['contact_person']
        ]);

        User::where('id', auth()->user()->id)->update([
            'team_id' => $team->id
        ]);

        array_push($this->response, [
            'nama_team' => $team->nama_team
        ]);

        return $team;
    }

    public function ketuaStore($ketuaParams)
    {
        $rules = [
            'team_id' => 'required',
            'nama_lengkap1' => 'required',
            'nim1' => 'required',
            'jurusan1' => 'required',
            'no_hp1' => 'required',
            'link_twibbon1' => 'required',
            'email1' => 'required|email|unique:ketuas,email',
            'ktm1' => 'required|image|max:2048',
            'pas_photo1' => 'required|image|max:2048',
        ];

        $validator = Validator::make($ketuaParams, $rules, $this->message);

        if($validator->fails()){
            $this->err['status'] = 'error';
            $this->err['message'] = $validator->errors()->first();
            return $this->err;
        }

        $pas_photo_name = 'pas_photo-' . str_replace(' ', '', $ketuaParams['nama_lengkap1']) . '-' . str_replace(' ', '', $ketuaParams['team_id']) . Str::random(6) . '.' .$ketuaParams['pas_photo1']->extension();
        // $ketuaParams['pas_photo1']->move(public_path('images'), $pas_photo_name);

        $ktm_name = 'ktm-' . str_replace(' ', '', $ketuaParams['nama_lengkap1']) . '-' . str_replace(' ', '', $ketuaParams['team_id']). Str::random(6) . '.' .$ketuaParams['ktm1']->extension();
        // $ketuaParams['ktm1']->move(public_path('images'), $ktm_name);

        $ketua = Ketua::create([
            'team_id' => $ketuaParams['team_id'],
            'nama_lengkap' => $ketuaParams['nama_lengkap1'],
            'nim' => $ketuaParams['nim1'],
            'jurusan' => $ketuaParams['jurusan1'],
            'no_hp' => $ketuaParams['no_hp1'],
            'email' => $ketuaParams['email1'],
            'ktm' => $ktm_name,
            'pas_photo' => $pas_photo_name,
            'link_twibbon' => $ketuaParams['link_twibbon1'],
        ]);

        array_push($this->response, [
            'ketua' => $ketua->nama_lengkap
        ]);

        return $ketua;
    }

    public function anggotaStore($anggota1, $anggota2 = null)
    {
        $rules = [
            'nama_lengkap2' => 'required',
            'nim2' => 'required',
            'jurusan2' => 'required',
            'no_hp2' => 'required',
            'link_twibbon2' => 'required',
            'email2' => 'required|email|unique:ketuas,email',
            'ktm2' => 'required|image|max:2048',
            'pas_photo2' => 'required|image|max:2048',
        ];
        $validator = Validator::make($anggota1, $rules, $this->message);
        if($validator->fails()){
            $this->err['status'] = 'error';
            $this->err['message'] = $validator->errors()->first();
            return $this->err;
        }
        $pas_photo_name = 'pas_photo-' . str_replace(' ', '', $anggota1['nama_lengkap2']) . '-' . str_replace(' ', '', $anggota1['ketua_id2']) . Str::random(6) . '.' .$anggota1['pas_photo2']->extension();
        // $anggota1['pas_photo2']->move(public_path('images'), $pas_photo_name);
        $ktm_name = 'ktm-' . str_replace(' ', '', $anggota1['nama_lengkap2']) . '-' . str_replace(' ', '', $anggota1['ketua_id2']) . Str::random(6) . '.' .$anggota1['ktm2']->extension();
        // $anggota1['ktm2']->move(public_path('images'), $ktm_name);

        $anggota1 = Anggota::create([
            'ketua_id' => $anggota1['ketua_id2'],
            'nama_lengkap' => $anggota1['nama_lengkap2'],
            'nim' => $anggota1['nim2'],
            'jurusan' => $anggota1['jurusan2'],
            'no_hp' => $anggota1['no_hp2'],
            'email' => $anggota1['email2'],
            'ktm' => $ktm_name,
            'pas_photo' => $pas_photo_name,
            'link_twibbon' => $anggota1['link_twibbon2'],
        ]);

        if($anggota2 != null){
            $rules = [
                'nama_lengkap3' => 'required',
                'nim3' => 'required',
                'jurusan3' => 'required',
                'no_hp3' => 'required',
                'link_twibbon3' => 'required',
                'email3' => 'required|email|unique:ketuas,email',
                'ktm3' => 'required|image|max:2048',
                'pas_photo3' => 'required|image|max:2048',
            ];
            $validator = Validator::make($anggota2, $rules, $this->message);
            if($validator->fails()){
                $this->err['status'] = 'error';
                $this->err['message'] = $validator->errors()->first();
                return $this->err;
            }

            $pas_photo_name = 'pas_photo-' . str_replace(' ', '', $anggota2['nama_lengkap3']) . '-' . str_replace(' ', '', $anggota2['ketua_id3']) . Str::random(6) . '.' .$anggota2['pas_photo3']->extension();
            // $anggota2['pas_photo3']->move(public_path('images'), $pas_photo_name);
            $ktm_name = 'ktm-' . str_replace(' ', '', $anggota2['nama_lengkap3']) . '-' . str_replace(' ', '', $anggota2['ketua_id3']) . Str::random(6) . '.' .$anggota2['ktm3']->extension();
            // $anggota2['ktm3']->move(public_path('images'), $ktm_name);

            $anggota2 = Anggota::create([
                'ketua_id' => $anggota2['ketua_id3'],
                'nama_lengkap' => $anggota2['nama_lengkap3'],
                'nim' => $anggota2['nim3'],
                'jurusan' => $anggota2['jurusan3'],
                'no_hp' => $anggota2['no_hp3'],
                'email' => $anggota2['email3'],
                'ktm' => $ktm_name,
                'pas_photo' => $pas_photo_name,
                'link_twibbon' => $anggota2['link_twibbon3'],
            ]);

            array_push($this->response, [
                'anggota1' => $anggota1->nama_lengkap,
                'anggota2' => $anggota2->nama_lengkap
            ]);

            return [
                $anggota1,
                $anggota2
            ];
        }else{
            array_push($this->response, [
                'anggota1' => $anggota1->nama_lengkap
            ]);

            return [
                $anggota1
            ];
        }
    }

    public function karyaTulisStore($karyaTulisParams)
    {
        $rules = [
            'team_id' => 'required',
            'judul_karya_tulis' => 'required',
            'subtema' => 'required',
            'file_abstrak' => 'required|mimes:pdf'
        ];

        $validator = Validator::make($karyaTulisParams, $rules, $this->message);

        if($validator->fails()){
            $this->err['status'] = 'error';
            $this->err['message'] = $validator->errors()->first();
            return $this->err;
        }

        $file_abstrak_name = 'abstrak-' . str_replace(' ', '', $karyaTulisParams['judul_karya_tulis']) . '-' . $karyaTulisParams['team_id'] . Str::random(6) . '.' . $karyaTulisParams['file_abstrak']->extension();
        // $karyaTulisParams['file_abstrak']->move(public_path('abstrak'), $file_abstrak_name);

        $karyaTulis = KaryaTulis::create([
            'team_id' => $karyaTulisParams['team_id'],
            'judul_karya_tulis' => $karyaTulisParams['judul_karya_tulis'],
            'subtema' => $karyaTulisParams['subtema'],
            'file_abstrak' => $file_abstrak_name
        ]);

        return $karyaTulis;
    }

    public function store(Request $request)
    {
        if(auth()->user()->team_id){
            return response()->json([
                'status' => 'error',
                'message' => 'Anda sudah pernah mendaftarkan team'
            ], 200);
        }

        DB::beginTransaction();
        try{
            $teamParams = [
                'nama_team' => $request->nama_team,
                'asal_institusi' => $request->asal_institusi,
                'jenis_lomba_id' => $request->jenis_lomba_id,
                'contact_person' => $request->contact_person
            ];
            $team = $this->teamStore($teamParams);
            if($this->err != []){
                return response($this->err, 400);
            }

            $ketuaParams = [
                'team_id' => $team->id,
                'nama_lengkap1' => $request->nama_lengkap1,
                'nim1' => $request->nim1,
                'jurusan1' => $request->jurusan1,
                'ktm1' => $request->ktm1,
                'pas_photo1' => $request->pas_photo1,
                'no_hp1' => $request->no_hp1,
                'email1' => $request->email1,
                'link_twibbon1' => $request->link_twibbon1,
            ];
            $ketua = $this->ketuaStore($ketuaParams);
            if($this->err != []){
                return response($this->err, 400);
            }

            $anggota1Params = [
                'ketua_id2' => $ketua->id,
                'nama_lengkap2' => $request->nama_lengkap2,
                'nim2' => $request->nim2,
                'jurusan2' => $request->jurusan2,
                'ktm2' => $request->ktm2,
                'pas_photo2' => $request->pas_photo2,
                'no_hp2' => $request->no_hp2,
                'email2' => $request->email2,
                'link_twibbon2' => $request->link_twibbon2,
            ];
            if($request->nama_lengkap3){
                $anggota2Params = [
                    'ketua_id3' => $ketua->id,
                    'nama_lengkap3' => $request->nama_lengkap3,
                    'nim3' => $request->nim3,
                    'jurusan3' => $request->jurusan3,
                    'ktm3' => $request->ktm3,
                    'pas_photo3' => $request->pas_photo3,
                    'no_hp3' => $request->no_hp3,
                    'email3' => $request->email3,
                    'link_twibbon3' => $request->link_twibbon3,
                ];
            }else{
                $anggota2Params = null;
            }
            $anggotas = $this->anggotaStore($anggota1Params, $anggota2Params);
            if($this->err != []){
                return response($this->err, 400);
            }

            // Bukti Foto
            $rules = [
                'bukti_share_wa' => 'required',
                'bukti_share_wa.*' => 'required|image|max:2048',
                'bukti_twibbon' => 'required',
                'bukti_twibbon.*' => 'required|image|max:2048',
                'bukti_follow_instagram' => 'required',
                'bukti_follow_instagram.*' => 'required|image|max:2048',
            ];

            $validator = Validator::make($request->all(), $rules, $this->message);
            if($validator->fails()){
                $this->err['status'] = 'error';
                $this->err['message'] = $validator->errors()->first();
                return response($this->err, 400);
            }

            $karyaTulisParams = [
                'team_id' => $team->id,
                'judul_karya_tulis' => $request->judul_karya_tulis,
                'subtema' => $request->subtema,
                'file_abstrak' => $request->file_abstrak,
            ];

            $karyaTulis = $this->karyaTulisStore($karyaTulisParams);
            if($this->err != []){
                return response($this->err, 400);
            }

            DB::commit();
        }catch( \Exception $e){
            DB::rollBack();
            throw $e;
        }
        // Upload foto dijalankan ketika semua data input ke db berhasil, sehingga kalau gagal foto ga ke upload
        $request->pas_photo1->move(public_path('images'), $ketua['pas_photo']);
        $request->ktm1->move(public_path('images'), $ketua['ktm']);
        if($request->nama_lengkap3){
            $request->pas_photo2->move(public_path('images'), $anggotas[0]['pas_photo']);
            $request->ktm2->move(public_path('images'), $anggotas[0]['ktm']);
            $request->pas_photo3->move(public_path('images'), $anggotas[1]['pas_photo']);
            $request->ktm3->move(public_path('images'), $anggotas[1]['ktm']);
        }else{
            $request->pas_photo2->move(public_path('images'), $anggotas[0]['pas_photo']);
            $request->ktm2->move(public_path('images'), $anggotas[0]['ktm']);
        }
        $request->file_abstrak->move(public_path('abstrak'), $karyaTulis['file_abstrak']);

        Bukti::create([
            'team_id' => $team->id
        ]);
        $waName = [];
        foreach($request->file('bukti_share_wa.*') as $file){
            $name = time().rand(1,100) . '.' . $file->extension();
            $file->move(public_path('images'), $name);
            array_push($waName, $name);
        }
        for($i=0; $i < count($waName); $i++){
            Bukti::where('team_id', $team->id)->update([
                'bukti_share_wa_' . $i => $waName[$i]
            ]);
        }

        $igName = [];
        foreach($request->file('bukti_twibbon.*') as $file){
            $name = time().rand(1,100) . '.' . $file->extension();
            $file->move(public_path('images'), $name);
            array_push($igName, $name);
        }
        for($i=0; $i < count($igName); $i++){
            Bukti::where('team_id', $team->id)->update([
                'bukti_twibbon_' . $i => $igName[$i]
            ]);
        }

        $twbName = [];
        foreach($request->file('bukti_follow_instagram.*') as $file){
            $name = time().rand(1,100) . '.' . $file->extension();
            $file->move(public_path('images'), $name);
            array_push($twbName, $name);
        }
        for($i=0; $i < count($twbName); $i++){
            Bukti::where('team_id', $team->id)->update([
                'bukti_follow_instagram_' . $i => $twbName[$i]
            ]);
        }

        return response()->json([
            'status'  => 'success',
            'message'  => 'Kamu berhasil mendaftarkan team',
            'data' => $this->response
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReactionRegistration  $reactionRegistration
     * @return \Illuminate\Http\Response
     */
    public function show(ReactionRegistration $reactionRegistration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReactionRegistration  $reactionRegistration
     * @return \Illuminate\Http\Response
     */
    public function edit(ReactionRegistration $reactionRegistration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateReactionRegistrationRequest  $request
     * @param  \App\Models\ReactionRegistration  $reactionRegistration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReactionRegistration  $reactionRegistration
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReactionRegistration $reactionRegistration)
    {
        //
    }
}
