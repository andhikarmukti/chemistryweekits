<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReactionTeam extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function registrations()
    {
        return $this->hasMany(ReactionRegistration::class, 'team_id', 'id');
    }

    public function ketua()
    {
        return $this->hasOne(Ketua::class, 'team_id', 'id');
    }

    public function buktis()
    {
        return $this->hasMany(Bukti::class, 'team_id', 'id');
    }

    public function karyaTulis()
    {
        return $this->hasOne(KaryaTulis::class, 'team_id', 'id');
    }

    public function pemenang()
    {
        return $this->hasOne(Pemenang::class, 'team_id', 'id');
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'team_id', 'id');
    }
}
