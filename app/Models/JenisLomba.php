<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisLomba extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->hasMany(User::class, 'id', 'jenis_lomba_id');
    }
}
