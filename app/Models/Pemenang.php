<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemenang extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function team()
    {
        return $this->hasOne(ReactionTeam::class, 'id', 'team_id');
    }
}
